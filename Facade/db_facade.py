import os
import pandas as pd
from sqlalchemy import create_engine, MetaData
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import sessionmaker

from operator import attrgetter


class DBFacade:

    def __init__(self):
        self.engine, self.metadata, self.session, self.Base = self.create_connection()

    def create_connection(self):
        """
        Function that initialize connection to the database
        :return: sqlalchemy objects
        """
        db_path = os.path.join(os.getcwd(), "project.db")
        db_path = 'sqlite:///' + db_path
        engine = create_engine(db_path)

        metadata = MetaData()
        metadata.reflect(engine)

        Base = automap_base(metadata=metadata)
        Base.prepare()

        DB_session = sessionmaker(bind=engine)
        session = DB_session()

        return engine, metadata, session, Base

    # def facade_manager(self):
    #
    #     engine, metadata, session, Base = create_engine()

    def get_tables(self):
        """
        Function that get tables name from database
        :return:
        """
        tables = list(self.metadata.tables.keys())
        return tables

    def get_columns(self, table_name):
        table = attrgetter(table_name)
        table = table(self.Base.classes)

        results = self.session.query(table).first()

        columns = list(results.__dict__.keys())[1:]

        return columns

    def get_headers(self, result):
        """
        Function that get headers from row in database query result
        :param result:
        :return:
        """
        return list(result.__dict__.keys())[1:]

    def building_results_array(self, headers, results):
        """
        Function prasing a bd query results inot a 2D array
        :param headers:
        :param results:
        :return:
        """
        results_array = [headers]
        for result in results:
            result_array = []
            for header in headers:
                header = attrgetter(header)
                result_array.append(header(result))
            results_array.append(result_array)

        results_array = pd.DataFrame(results_array)
        return results_array

    def prinitng_select_all(self, results):
        """
        Function that prints the results of a query
        :param results:
        :return:
        """
        print('\n')
        print(results.to_string(index=False, header=False))
        print('\n')

    def select_all(self, table_name):
        """
        Function that supports a 'select * from table_name' query
        :param table_name:
        :return:
        """
        table = attrgetter(table_name)
        table = table(self.Base.classes)

        results = self.session.query(table).all()

        # print('results: ', results)

        headers = self.get_headers(results[0])
        results_array = self.building_results_array(headers, results)
        self.prinitng_select_all(results_array)

    def select_by_criterion(self, table_name, criterion, answer):
        """
        Function that supports a select by criterion query
        :param table_name:
        :param criterion:
        :param answer:
        :return:
        """
        print('table_name: ', table_name, ' criterion: ', criterion, ' = ', answer)

        table = attrgetter(table_name)
        table = table(self.Base.classes)

        atribute = attrgetter(criterion)
        critertion = atribute(table)

        results = self.session.query(table).filter(critertion == answer)

        try:
            headers = self.get_headers(results[0])
            results_array = self.building_results_array(headers, results)
            self.prinitng_select_all(results_array)
        except:
            print('Nie znaleziono wyników.')

    def raw_results_printing(self, results):
        results_array = pd.DataFrame(results)
        print(results_array.to_string(index=False, header=False), '\n')

    def select_by_user_query(self, query):
        results = self.engine.execute(query)
        headers = tuple(results.keys())
        results = results.fetchall()

        results.insert(0, headers)

        self.raw_results_printing(results)

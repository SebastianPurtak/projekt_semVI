from Interface.cli_interface import CLIInterface
from Facade.db_facade import DBFacade

class MainController:
    """
    The class controlling the operation of the program
    """

    def __init__(self, args):
        self.args = args

    def main_controll(self):
        """
        The function controlling the program at the highest level of abstraction.
        Invokes lower-order controllers controlling specific modules.
        :return:
        """
        if self.args['run']:
            self.main_interface_loop()

    def build_user_choice_dict(self):
        """
        Function that prepares a dictionary with possible user responses
        :return:
        """
        interface = CLIInterface()
        facade = DBFacade()
        user_choice = {}

        tables_names = facade.get_tables()

        user_choice['Pokaż dostępne tabele'] = interface.show_tables_menu
        for table_name in tables_names:
            user_choice[table_name] = interface.show_table_menu

        user_choice['Powrót do menu głównego'] = interface.show_main_menu
        user_choice['Powrót do wyboru tabeli'] = interface.show_tables_menu
        user_choice['Wyświetl całą tabelę'] = interface.select_all
        user_choice['Wyszukiwanie według kryterium'] = interface.select_by_criterion
        user_choice['Wprowadź zapytanie'] = interface.user_query_interface

        return user_choice

    def main_interface_loop(self):
        interface = CLIInterface()
        facade = DBFacade()

        user_choices = self.build_user_choice_dict()

        interface.show_title()
        user_choice = interface.show_main_menu()

        while user_choice != 'Zakończ':
            if user_choice in ['Pokaż dostępne tabele', 'Powrót do wyboru tabeli']:
                tables = facade.get_tables()
                user_choice = user_choices[user_choice](tables)
            elif user_choice in ['Wyświetl całą tabelę', 'Wyszukiwanie według kryterium']:
                user_choice = user_choices[user_choice](table_name)
            elif user_choice in ['Powrót do menu głównego', 'Wprowadź zapytanie']:
                user_choice = user_choices[user_choice]()
            else:
                user_choice, table_name = user_choices[user_choice](user_choice)
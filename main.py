"""
SEM VI Project

Usage:
    main.py
    main.py -h|--help
    main.py -v|--version
    main.py run

Options:
    -h --help       Show this screen
    -v --version    Show version
    run             Required initialization command
"""

import docopt as dc

from Controller.main_controller import MainController
from Facade.db_facade import DBFacade
from Interface.cli_interface import CLIInterface


if __name__ == '__main__':
    args = dc.docopt(__doc__, version='0.0.1')

    main_controller = MainController(args)
    main_controller.main_controll()


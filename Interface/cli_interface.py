from pyfiglet import Figlet
from PyInquirer import prompt, print_json, style_from_dict, Token, Separator
from examples import custom_style_2
from pprint import pprint

from Facade.db_facade import DBFacade


class CLIInterface:

    def show_title(self):
        title = 'Semestr VI: program zaliczeniowy'

        f = Figlet(font='big', justify='center')
        print(f.renderText(title))

    def show_main_menu(self):
        questions = [
            {
                'type': 'list',
                'name': 'main_menu',
                'message': 'Co chcesz zrobić?',
                'choices': [
                    Separator(),
                    'Pokaż dostępne tabele',
                    'Wprowadź zapytanie',
                    'Zakończ',
                    Separator()
                ]
            },

        ]

        answer = prompt(questions, style=custom_style_2)
        # print('answer:', answer)

        return answer['main_menu']

    def show_tables_menu(self, tables):
        # tables = ['Gry', 'Deweloperzy', 'Wydawcy', 'Powrót do menu głównego']
        tables.append('Powrót do menu głównego')
        questions = {
            'type': 'list',
            'name': 'tables_menu',
            'message': 'Dostępne tabele:',
            'choices': tables
        }

        answer = prompt(questions, style=custom_style_2)

        return answer['tables_menu']

    def show_table_menu(self, table_name):
        questions = {
            'type': 'list',
            'name': 'table_menu',
            'message': 'Wybierz rodzaj wyszukiwania:',
            'choices': [
                Separator(),
                'Wyświetl całą tabelę',
                'Wyszukiwanie według kryterium',
                'Powrót do wyboru tabeli',
                Separator()
            ]
        }

        answer = prompt(questions, style=custom_style_2)

        return answer['table_menu'], table_name

    def criterion_serach(self, criterion, table_name, facade):
        print(criterion, '= ', end='', flush=True)
        answer = input()

        facade.select_by_criterion(table_name, criterion, answer)

        return 'Powrót do wyboru tabeli'

    def select_by_criterion(self, table_name):
        """
        CLI interface for select by criterion
        :param table_name:
        :return:
        """
        facede = DBFacade()
        criterion = facede.get_columns(table_name)
        criterion.append('Powrót do wyboru tabeli')
        questions = {
            'type': 'list',
            'name': 'table_menu',
            'message': 'Kryteria wyszukiwania:',
            'choices': criterion
        }

        # print('criterion: ', criterion[:-1])

        answer = prompt(questions, style=custom_style_2)

        # print('answer: ', answer)
        if answer['table_menu'] in criterion[:-1]:
            answer['table_menu'] = self.criterion_serach(answer['table_menu'], table_name, facede)

        return answer['table_menu']

    def select_all(self, table_name):
        """
        CLI interface for select all query
        :param table_name:
        :return:
        """
        facade = DBFacade()
        facade.select_all(table_name)

        answer, table_name = self.show_table_menu(table_name)

        return answer

    def user_query_interface(self):
        facade = DBFacade()

        print('Wprowadź zapytanie:\n')
        query = input()
        print('Zapytanie: ', query, '\n')

        facade.select_by_user_query(query)

        return 'Powrót do menu głównego'
